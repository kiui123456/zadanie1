﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Zadanie1.Controllers;
namespace Zadanie1.Controllers
{
     class Circle : Shape
    {
        override public double area() {
            return Math.PI * Math.Pow(a, 2);
    }
    }
}