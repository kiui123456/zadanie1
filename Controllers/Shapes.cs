﻿using System.Text.RegularExpressions;
using System.Web.Mvc;
using Zadanie1.Models;

namespace Zadanie1.Controllers
{
    internal class Shapes : Shape
    {
        private dataShape collection;
        public bool err = false;
        public Shapes(dataShape collection):base()
        {
            this.collection = collection;
           
        }
        public bool expOne(string input)
        {

            string pattern = @"\[[+-]?([0-9]*[.])?[0-9]+\]";

            RegexOptions options = RegexOptions.Multiline;

            foreach (Match m in Regex.Matches(input, pattern, options))
            {
                System.Console.WriteLine("'{0}' found at index {1}.", m.Value, m.Index);
            }
            return true;
        }
        public bool expTwo(string input )
        {
            string pattern = @"\[[[+-]?([0-9]*[.])?[0-9]+[,][[+-]?([0-9] *[.])?[0-9]\]";

            RegexOptions options = RegexOptions.Multiline;

            foreach (Match m in Regex.Matches(input, pattern, options))
            {
                //Console.WriteLine("'{0}' found at index {1}.", m.Value, m.Index);
            }
            return true;
        }
        public reply result()
        {
            
            switch (this.collection.type)
            {
                case "1":
                    this.err = expOne(this.collection.data);
                    if (!err)
                    {
                        return new reply() { area = base.area(), msg = "" };
                    }
                    else
                    {
                        return new reply() { area = 0, msg = "Odpowiedź w danym formacie jest niepoprawna. Poprawny format to [a] np. [2.2]", err = err };
                    }
                    break;
                case "2":
                    this.err = expOne(this.collection.data);
                    if (!err)
                    {
                        return new reply() { area = new Circle().area(), msg = "" };
                    }
                    else
                    {
                        return new reply() { area = 0, msg = "Odpowiedź w danym formacie jest niepoprawna. Poprawny format to [r] np. [4.2]", err = err };
                    }
                    break;
                case "3":
                    this.err = expTwo(this.collection.data);
                    if (!err)
                    {
                        return new reply() { area = new Triangle().area(), msg = "" };
                    }
                    else
                    {
                        return new reply() { area = 0, msg = "Odpowiedź w danym formacie jest niepoprawna. Poprawny format to [a,h] np. [2.5,9.8]", err = err };
                    }
                    break;
                case "4":
                    this.err = expTwo(this.collection.data);
                    if (!err)
                    {
                        return new reply() { area = new Rectangle().area(), msg = "" };
                    }
                    else
                    {
                        return new reply() { area = 0, msg = "Odpowiedź w danym formacie jest niepoprawna. Poprawny format to [a,h] np. [2.5,9.8]", err = err };
                    }
                    break;
                default:
                    return new reply() { area = 0, msg = "Odpowiedź w danym formacie jest niepoprawna. Poprawny format to [a] np. [2.2]", err = err };
            }


            
        }
    }
}