﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Zadanie1.Models;
using Zadanie1.Views;

namespace Zadanie1.Controllers
{
   
    public class ShapeApiController : ApiController
    {
        // GET: api/ShapeApi
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/ShapeApi/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/ShapeApi
        public void Post([FromBody]dataShape value)
        {
            Shapes shape = new Shapes(value);
        }

        // PUT: api/ShapeApi/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/ShapeApi/5
        public void Delete(int id)
        {
        }
    }
}
